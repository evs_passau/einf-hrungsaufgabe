#!/bin/sh


latex VerschlJava8.tex && latex VerschlJava8.tex && dvipdf VerschlJava8.dvi Verschlüsselung_in_Java_8.pdf
rm textput.log
rm VerschlJava8.aux
rm VerschlJava8.log
rm VerschlJava8.dvi
rm VerschlJava8.out
rm VerschlJava8.snm
rm VerschlJava8.toc
rm VerschlJava8.vrb
rm VerschlJava8.nav
